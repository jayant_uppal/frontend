import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/shared/models/User.model';
import { DismissiveAlertComponent } from '../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  user: User = {
    userName: {
      firstName: '',
      middleName: '',
      lastName: '',
    },
    mobileNumber: '',
    email: '',
    password:''
  };

  //Alert Component whose add method is gonna be accesed here
  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  onRegister() {
    console.log("user",this.user);
    this.userService.registerUser(this.user).subscribe({
      next: (res) => {
        console.log(res);
        this.alert.add('success', 'Registered Successfully', 5000);
        //redirection
        //redirect to login page
        this.router.navigate(['login']);
      },
      error: (err) => {
        console.log(err);
        this.alert.add('danger', 'A problem occured while registering', 3000);
      },
      complete: () => console.info('complete'),
    });
  }
}
