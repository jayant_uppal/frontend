import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegeDetailFormComponent } from './college-detail-form.component';

describe('CollegeDetailFormComponent', () => {
  let component: CollegeDetailFormComponent;
  let fixture: ComponentFixture<CollegeDetailFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollegeDetailFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeDetailFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
