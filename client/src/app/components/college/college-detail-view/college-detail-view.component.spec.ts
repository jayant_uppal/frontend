import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegeDetailViewComponent } from './college-detail-view.component';

describe('CollegeDetailViewComponent', () => {
  let component: CollegeDetailViewComponent;
  let fixture: ComponentFixture<CollegeDetailViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollegeDetailViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
