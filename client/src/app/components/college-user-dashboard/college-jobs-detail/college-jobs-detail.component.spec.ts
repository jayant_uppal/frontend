import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollegeJobsDetailComponent } from './college-jobs-detail.component';

describe('CollegeJobsDetailComponent', () => {
  let component: CollegeJobsDetailComponent;
  let fixture: ComponentFixture<CollegeJobsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollegeJobsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeJobsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
