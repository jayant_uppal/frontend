export enum jobOpenFor {
  ALL,
  MALE_ONLY,
  FEMALE_ONLY,
}
